namespace Redis

open Print.Print
open StackExchange.Redis
open System
open Types.Types

/// <summary>
///     Module for working with a Redis server.
///     Provides methods for flushing the cache.
/// </summary>
module Redis =

    /// <summary>
    ///     Create a connection to a Redis server using a specified connection string.
    /// </summary>
    /// <param name="connectionString">Connection string to use when connecting.</param>
    /// <returns>Ok with a connection, otherwise Error with an error message.</returns>
    let private connect (connectionString: string) =
        try
            match ConnectionMultiplexer.Connect(connectionString) with
            | connection when connection.IsConnected -> Ok(connection)
            | _ -> Error("Not connected despite successful attempt")
        with
        | ex -> Error(ex.Message)

    /// <summary>
    ///     Flush a Redis server.
    /// </summary>
    /// <param name="server">Server to flush.</param>
    /// <returns>Flush result.</returns>
    let private flushServer (server: IServer) =
        match server with
        | s when s.IsReplica -> Skipped
        | s ->
            try
                s.FlushAllDatabases()
                Flushed
            with
            | ex -> Failed(ex.Message)

    /// <summary>
    ///     Get a list of servers from a Redis connection.
    /// </summary>
    /// <param name="connection">Redis connection to get servers from.</param>
    /// <returns>Ok with a list of servers, otherwise Error with an error message.</returns>
    let private getServers (connection: ConnectionMultiplexer) =
        try
            Ok(connection.GetServers() |> Array.toList)
        with
        | ex -> Error(ex.Message)

    /// <summary>
    ///     Flush Redis cache using a specified connection.
    /// </summary>
    /// <param name="connection">Connection to use when flushing Redis.</param>
    let private flushConnection (connection: ConnectionMultiplexer) =
        match getServers connection with
        | Ok servers ->
            servers
            |> List.map flushServer
            |> List.iter2 printFlush servers
        | Error message -> printfn "Failed to get servers. %s" message

    /// <summary>
    ///     Flush Redis cache using a specified connection string.
    /// </summary>
    /// <param name="connectionString">Connection string to use to connect to and flush Redis.</param>
    let flush (connectionString: string) =
        printfn "Connecting to Redis..."

        match connect connectionString with
        | Ok connection -> flushConnection connection
        | Error message -> cprintfn ConsoleColor.Red "Failed to connect. %s" message
