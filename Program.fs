﻿open Print.Print
open Redis

/// <summary>
///     Active pattern for matching program arguments.
/// </summary>
/// <returns>Matched pattern.</returns>
let (|Ok|Invalid|) (args: string []) =
    match args with
    | [| _ |] -> Ok
    | _ -> Invalid

/// <summary>
///     Application entry point.
/// </summary>
/// <returns>Exit Code.</returns>
[<EntryPoint>]
let main args =
    match args with
    | Ok -> Redis.flush args[0]
    | Invalid -> printHelp

    0
