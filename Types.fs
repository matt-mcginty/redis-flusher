namespace Types

/// <summary>
///     Module containing custom types used by the application.
/// </summary>
module Types =

    /// <summary>
    ///     Possible results from attempting to clear Redis cache.
    /// </summary>
    type FlushResult =
        | Skipped
        | Flushed
        | Failed of string
