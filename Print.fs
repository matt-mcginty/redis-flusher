namespace Print

open System
open System.Net
open StackExchange.Redis
open Types.Types

/// <summary>
///     Module for printing messages to the user.
/// </summary>
module Print =
    /// <summary>
    ///     Set the current Console foreground colour.
    /// </summary>
    /// <param name="fc">Foreground colour to set.</param>
    /// <returns>An IDisposable which resets the console colour upon dispose.</returns>
    let private consoleColor (fc: ConsoleColor) =
        let current = Console.ForegroundColor
        Console.ForegroundColor <- fc

        { new IDisposable with
            member x.Dispose() = Console.ForegroundColor <- current }

    /// <summary>
    ///     Format an endpoint object as a readable string.
    /// </summary>
    /// <param name="endpoint"></param>
    /// <returns>Formatted string.</returns>
    let private formatEndpoint (endpoint: EndPoint) =
        match endpoint with
        | :? DnsEndPoint as x -> sprintf "%s:%s" x.Host (x.Port.ToString())
        | _ -> endpoint.ToString()

    /// <summary>
    ///     Print a colour message to the console, followed by a newline.
    /// </summary>
    /// <param name="color">Console color to print with.</param>
    /// <param name="str">Message to print</param>
    let cprintfn color str =
        Printf.kprintf (fun s -> use c = consoleColor color in printfn "%s" s) str

    /// <summary>
    ///     Print a formatted message based on a Redis flush result.
    /// </summary>
    /// <example>
    ///     Flushed -> localhost:6379
    /// </example>
    /// <param name="server">Server that the flush was performed on.</param>
    /// <param name="flushResult">Result of the flush.</param>
    let printFlush (server: IServer) (flushResult: FlushResult) =
        match flushResult with
        | Skipped -> cprintfn ConsoleColor.DarkGray "Skipped (Replica) -> %s" (formatEndpoint server.EndPoint)
        | Flushed -> cprintfn ConsoleColor.Green "Flushed -> %s" (formatEndpoint server.EndPoint)
        | Failed message ->
            cprintfn ConsoleColor.Red "Failed to flush -> %s (%s)" (formatEndpoint server.EndPoint) message

    /// <summary>
    ///     Print help text to console.
    /// </summary>
    let printHelp =
        cprintfn ConsoleColor.Gray "To flush Redis Cache, you must enter the connection string of the instance."
        cprintfn ConsoleColor.Yellow "Connection string must start with the desired server and include allowAdmin=true."

        cprintfn
            ConsoleColor.Gray
            """
Example command (Non-Sentinel):
./redis-flusher.exe localhost,connectRetry=1,connectTimeout=3000,allowAdmin=true

Example command (Sentinel):
./redis-flusher.exe 10.100.30.31:26379,10.100.30.32:26379,10.100.30.33:26379,serviceName=st-master,connectRetry=5,connectTimeout=3000,allowAdmin=true"""
