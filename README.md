# redis-flusher

Small F# application for flushing Redis cache

Supports both non-Sentinel and Sentinel connection strings 

In a replicated cluster, only the master will be flushed

## Getting started

Running the application with blank or invalid arguments will print a help message with example commands:

![Help message](https://gitlab.com/matt-mcginty/redis-flusher/-/raw/main/assets/example-help.png)

## Examples

Running the application on a non-Sentinel redis connection:

![Non-Sentinel](https://gitlab.com/matt-mcginty/redis-flusher/-/raw/main/assets/example-non-sentinel.png)

Running the aplication on a Sentinel redis cluster with 3 nodes (Note that the replicas are skipped):

![Sentinel](https://gitlab.com/matt-mcginty/redis-flusher/-/raw/main/assets/example-sentinel.png)
